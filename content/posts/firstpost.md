---
title: "First blog post"
date: 2022-04-16T21:33:13-04:00
draft: false
---

Introduction
-----

Hello! I just started this new blog to keep track of my compsci abilities. I remember back in 2019 I started a hugo website for my organ-playing adventures. Since then I switched to building a website from scratch using Go. 

Plans
---

I'm hoping that this blog will also be a reference for the different projects that I'm working on. Here are those which I'm working on at the moment.

- [codeberg.com/FiskFan1999/wvlist.net](https://codeberg.org/FiskFan1999/wvlist): This is the most time-consuming project so far. I started this in January during Winter break, and since the beginning of the spring semester have not worked on it as much. This website is a database of lists of compositions by composers in such a way that a composition can be referenced by an index. One example of this is the BWV numbers for Johann Sebastian Bach. In the future, I am planning to add some rudimentary federation to this service, so that a website can display listings that are hosted on other instances.
- [github.com/ObieSource/peertube-multipart-upload](https://github.com/ObieSource/peertube-multipart-upload): This project is my implementation of multipart-upload to peertube via the API. The application is configured with environmental variables. This project is an excercise in writing useful unit tests to ensure the code is working correctly. This project is currently very close to stable, and may be missing a few features that I want to add.
- [github.com/ObieSource/FiskFansIRCBot](https://github.com/ObieSource/FiskFansIRCBot): This is inspired by the discord bot that I made a few years ago. It is being re-written in Go and for IRC instead, but will include many of the same features.
- [codeberg.org/FiskFan1999/fedibot](https://codeberg.org/FiskFan1999/fedibot): Another IRC bot, this one will be used to recieve notifications about Peertube uploads in IRC channels (will add other fediverse platforms as well).

Conclusion
---

Hopefully that will give you a good idea about where I stand in my adventures in computer programming. Please feel free to chat with me on IRC and become friends on the fediverse!

