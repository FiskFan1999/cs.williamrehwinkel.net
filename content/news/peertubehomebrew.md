---
title: "peertube-multipart-upload available on homebrew"
date: 2022-07-14T01:50:16-04:00
draft: true
---

Peertube Multipart Upload can now be installed via homebrew, for those of you who use this package manager.

It is the first formula on my new personal homebrew tap, which is identified as [FiskFan1999/FiskFan1999](https://codeberg.org/FiskFan1999/homebrew-FiskFan1999) and hosted on codeberg as opposed to github (note the `brew tap` command below).

The [instructions to install peertube-multipart-upload via homebrew](https://github.com/ObieSource/peertube-multipart-upload#installation) are as follows:

```bash
brew tap FiskFan1999/FiskFan1999 https://codeberg.org/FiskFan1999/homebrew-FiskFan1999.git
brew install peertube-multipart-upload
```
