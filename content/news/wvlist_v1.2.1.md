---
title: "wvlist v1.2.1 released"
date: 2022-04-18T17:14:04-04:00
draft: true
---

Today I pushed out a [new release on Codeberg](https://codeberg.org/FiskFan1999/wvlist/releases/tag/v1.2.1) that fixes a bug in which compiling wvlist with an empty value for Commit causes a panic when serving the homepage.

[Found here](https://codeberg.org/FiskFan1999/wvlist/src/commit/9e98f634836c9456086aedd707ad1c57044d9b88/linkToRepo.go#L22-L32)
{{< highlight go >}}
func GetLinkToCommitInRepositry(commit string) (html string, snippet string) {
    /*
        if commit == "" then panics
     */
	buf := new(bytes.Buffer)

	fmt.Fprintf(buf, "%s/commit/%s", LinkToRepository, commit)
	html = buf.String()

	snippet = commit[:9] + "..." // index out of bounds error

	return

}
{{< /highlight >}}

This bug was encountered during the process of moving the wvlist working directory from one vm to another during my process of consolidating my web applications all on one vm.

I have an update for entering the admin password for hasing in the CLI ready to roll out soon.
